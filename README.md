# Simple Python flask webapp

## Install dependencies

```bash
pip install --no-cache-dir -r requirements.txt
```

## Launch the application

```bash
python ./src/app.py
```

## This was made by William Der Krikorian in M1 DEV for Microservices exam